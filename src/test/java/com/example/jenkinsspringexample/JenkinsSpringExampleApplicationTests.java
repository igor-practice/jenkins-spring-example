package com.example.jenkinsspringexample;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JenkinsSpringExampleApplicationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testGetEndpoint() {
        ResponseEntity<String> response = testRestTemplate.exchange(
                "/",
                HttpMethod.GET,
                null,
                String.class
        );
        assertThat(response.getBody()).contains("Elo mordeczko");
    }
}
